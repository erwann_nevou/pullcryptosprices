import calendar
import json
from datetime import datetime

import requests

# Get the coin list to map symbols to coingecko ids
idsRequest = requests.get("https://api.coingecko.com/api/v3/coins/list")

# Save them in a file for debug
# idsFile = open("ids.json", mode="w", encoding="utf-8")
# idsFile.write(idsRequest.text)
# idsFile.close()

idsJSON = json.loads(idsRequest.content)

# Read the conf file
confFile = open("conf.txt", mode="r")
confText = confFile.read()
confLines = confText.split("\n")
startDate = datetime.strptime(confLines[0], '%d-%m-%Y')
tokens = confLines[1:len(confLines)]

correspondingIds = list(filter(lambda i : (i["symbol"] in tokens or i["id"] in tokens) and "binance-peg" not in i["id"] and "wormhole" not in i["id"] and "wrapped" not in i["id"], idsJSON))

outputFile = open("prices.csv", mode="a+", encoding="utf-8")
outputFile.seek(0)
outputFileContent = outputFile.read()

today = datetime.now()

# For each month from startDate to today
for year in range(startDate.year, today.year + 1) :
    startMonth = startDate.month if year == startDate.year else 1
    endMonth = today.month if year == today.year else 13
    for month in range(startMonth, endMonth) :
        lastMonthDay = calendar.monthrange(year, month)[1]
        dateString = datetime(year, month, lastMonthDay).strftime("%d-%m-%Y")
        # For each token
        for id in correspondingIds :
            tokenId = id["id"]
            # Get token price in EUR and USD if not already in file
            if f"{dateString},{tokenId}" not in outputFileContent :
                url = f"https://api.coingecko.com/api/v3/coins/{tokenId}/history?date={dateString}"
                r = requests.get(url)
                content = json.loads(r.content)
                if "market_data" in content:
                    prices = content["market_data"]["current_price"]
                    priceEUR = prices["eur"]
                    priceUSD = prices["usd"]
                    outputFile.write(f"{dateString},{tokenId},{priceEUR},{priceUSD}\n")
                    print(dateString, tokenId)

outputFile.close()
