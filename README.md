# PullCryptosPrices

Small script to retrieve tokens prices in EUR & USD for every month from a start date to today. For each month we select the prices for the last day of the month. The prices can be found in the ouput file 'prices.csv'.

It uses the coingecko API : https://www.coingecko.com/api/documentations/v3..

Note: The API limits the number of calls / minute to 100. The script does not take this limit into account. You may need to relaunch the script a minute later if it crashes.

## Installation
1. [Install python](https://www.python.org/downloads/)
2. Execute following commands : 
```shell
# Create virtual environment 
py -3 -m venv .venv
# Temporarily change the PowerShell execution policy to allow scripts to run
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process
# Activate virtual environment 
.venv\scripts\activate
# Install package requests
python -m pip install requests
# Launch
python .\pullCryptoPrices.py
.\.venv\Scripts\python.exe .\pullCryptoPrices.py
```

## Configuration
All the configuration is in 'conf.txt'.
First line is the starting date.
Next lines are the symbols of the tokens.